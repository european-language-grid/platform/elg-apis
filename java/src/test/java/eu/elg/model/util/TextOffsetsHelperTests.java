/*
 *    Copyright 2022 The European Language Grid
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package eu.elg.model.util;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class TextOffsetsHelperTests {

  // This is a test <smiley face><three orange hearts> more text <EU flag>.
  final TextOffsetsHelper helper = new TextOffsetsHelper(
          "This is a test \uD83D\uDE00\uD83E\uDDE1\uD83E\uDDE1 more text \uD83C\uDDEA\uD83C\uDDFA.");

  @ParameterizedTest
  @CsvSource({
          "0, 0", "4, 4", "15, 15", "16, 17", "18, 21", "19, 22", "29, 32", "31, 36", "32, 37"
  })
  public void testElgToJava(int from, int to) {
    assertEquals(to, helper.elgToJava(from));
  }

  @ParameterizedTest
  @CsvSource({
          "0, 0", "4, 4", "15, 15", "17, 16", "21, 18", "22, 19", "32, 29", "36, 31", "37, 32"
  })
  public void testJavaToElg(int from, int to) {
    assertEquals(to, helper.javaToElg(from));
  }

}
