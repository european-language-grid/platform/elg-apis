/*
 *    Copyright 2019 The European Language Grid
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package eu.elg.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import eu.elg.model.responses.AnnotationsResponse;
import eu.elg.model.responses.TextsResponse;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Representation of a single standoff annotation with respect to either one or two streams of data.  When used as part
 * of an {@link AnnotationsResponse} the "source" offsets are not used, and the plain "start" and "end" properties refer
 * to the annotation's location within the data that was being annotated.  When used with a {@link TextsResponse} there
 * are two streams of data, the "source" that was passed to the service (e.g. a text to be translated) and the "result"
 * embedded in the {@link TextsResponse} (e.g. the target language translation).  In this case the "start" and "end"
 * refer to the result, and "sourceStart" and "sourceEnd" refer to the source data.
 */
public class AnnotationObject {

  private Number start;

  private Number end;

  private Number sourceStart;

  private Number sourceEnd;

  private Map<String, Object> features;

  /**
   * Start offset of this annotation.  In the case of services like translation where there are two separate data
   * streams (the source and the result data) this is the offset with respect to the result.  If this annotation links
   * to specific locations in both the result and the source streams, the source offset is {@link #getSourceStart()}.
   */
  @JsonProperty("start")
  public Number getStart() {
    return start;
  }

  /**
   * Set the start offset of this annotation - see {@link #getStart()} for more details.
   *
   * @param start the start offset
   */
  @JsonProperty("start")
  public void setStart(Number start) {
    this.start = start;
  }

  /**
   * Fluent setter for the {@link #getStart start property}
   *
   * @param start the start offset
   * @return this object, for chaining
   */
  public AnnotationObject withStart(Number start) {
    setStart(start);
    return this;
  }

  /**
   * End offset of this annotation.  In the case of services like translation where there are two separate data streams
   * (the source and the result data) this is the offset with respect to the result.  If this annotation links to
   * specific locations in both the result and the source streams, the source offset is {@link #getSourceEnd()}.
   */
  @JsonProperty("end")
  public Number getEnd() {
    return end;
  }

  /**
   * Set the end offset of this annotation - see {@link #getEnd()} for more details.
   *
   * @param end the end offset
   */
  @JsonProperty("end")
  public void setEnd(Number end) {
    this.end = end;
  }

  /**
   * Fluent setter for the {@link #getEnd end property}.
   *
   * @param end the end offset
   * @return this object, for chaining
   */
  public AnnotationObject withEnd(Number end) {
    setEnd(end);
    return this;
  }

  /**
   * Shorthand fluent setter for both the start and end offsets.
   *
   * @param start start offset
   * @param end   end offset
   * @return this object, for chaining
   */
  public AnnotationObject withOffsets(Number start, Number end) {
    setStart(start);
    setEnd(end);
    return this;
  }

  /**
   * Start offset of the annotation with respect to the source data (e.g. the source language in the case of
   * translation).  May be null if there is no separate source and target data stream in this context (e.g. for
   * information extraction).
   */
  @JsonProperty("sourceStart")
  public Number getSourceStart() {
    return sourceStart;
  }

  /**
   * Set the start offset of this annotation with respect to the source data.
   *
   * @param sourceStart
   */
  @JsonProperty("sourceStart")
  @JsonInclude(JsonInclude.Include.NON_NULL)
  public void setSourceStart(Number sourceStart) {
    this.sourceStart = sourceStart;
  }

  /**
   * Fluent setter for the {@link #getSourceStart sourceStart property}
   *
   * @param sourceStart start offset in the source data
   * @return this object, for chaining
   */
  public AnnotationObject withSourceStart(Number sourceStart) {
    setSourceStart(sourceStart);
    return this;
  }

  /**
   * End offset of the annotation with respect to the source data (e.g. the source language in the case of translation).
   *  May be null if there is no separate source and target data stream in this context (e.g. for information
   * extraction).
   */
  @JsonProperty("sourceEnd")
  public Number getSourceEnd() {
    return sourceEnd;
  }

  /**
   * Set the end offset of this annotation with respect to the source data.
   *
   * @param sourceEnd end offset in the source data.
   */
  @JsonProperty("sourceEnd")
  @JsonInclude(JsonInclude.Include.NON_NULL)
  public void setSourceEnd(Number sourceEnd) {
    this.sourceEnd = sourceEnd;
  }

  /**
   * Fluent setter for the {@link #getSourceEnd sourceEnd property}
   *
   * @param sourceEnd end offset in the source data
   * @return this object, for chaining
   */
  public AnnotationObject withSourceEnd(Number sourceEnd) {
    setSourceEnd(sourceEnd);
    return this;
  }

  /**
   * Shorthand fluent setter for both the start and end source offsets.
   *
   * @param start start offset in the source data
   * @param end   end offset in the source data
   * @return this object, for chaining
   */
  public AnnotationObject withSourceOffsets(Number start, Number end) {
    setSourceStart(start);
    setSourceEnd(end);
    return this;
  }

  /**
   * Features of this annotation.  The keys in the map must be strings, the values can be anything Jackson knows how to
   * serialise to JSON.
   */
  @JsonProperty("features")
  public Map<String, Object> getFeatures() {
    return features;
  }

  /**
   * Set features for this annotation.The keys in the map must be strings, the values can be anything Jackson knows how
   * to serialise to JSON.
   *
   * @param features map of features to set
   */
  @JsonProperty("features")
  @JsonInclude(JsonInclude.Include.NON_EMPTY)
  public void setFeatures(Map<String, Object> features) {
    this.features = features;
  }

  /**
   * Fluent setter for the {@link #getFeatures features property}
   *
   * @param features map of features to set
   * @return this object, for chaining.
   */
  public AnnotationObject withFeatures(Map<String, Object> features) {
    setFeatures(features);
    return this;
  }

  /**
   * Shorthand way to set features - this method takes an alternating list of key1, value1, key2, value2, etc.  Keys
   * must be strings, values can be anything Jackson knows how to serialise to JSON.
   *
   * @param keysAndValues alternating list of key, value, key, value, ...
   * @return this object, for chaining
   */
  public AnnotationObject withFeatures(Object... keysAndValues) {
    if(keysAndValues.length % 2 != 0) {
      throw new IllegalArgumentException("argument must be alternating keys and values");
    }
    Map<String, Object> featuresMap = new LinkedHashMap<>();
    for(int i = 0; i < keysAndValues.length; i += 2) {
      featuresMap.put((String) keysAndValues[i], keysAndValues[i + 1]);
    }
    return withFeatures(featuresMap);
  }

  /**
   * Convenience fluent setter for a single key/value feature pair.  This method calls {@link Map#put(Object, Object)
   * put} on the current {@link #getFeatures() features} map, creating a new {@link LinkedHashMap} if this is null.  If
   * the current features map is not mutable then this call may fail.
   *
   * @param key   the feature key
   * @param value the feature value (can by any type that Jackson knows how to serialise)
   * @return this object, for chaining.
   */
  public AnnotationObject withFeature(String key, Object value) {
    if(getFeatures() == null) {
      setFeatures(new LinkedHashMap<>());
    }
    getFeatures().put(key, value);
    return this;
  }
}
