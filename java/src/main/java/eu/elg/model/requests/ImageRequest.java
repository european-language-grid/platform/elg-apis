/*
 *    Copyright 2019 The European Language Grid
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package eu.elg.model.requests;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import eu.elg.model.Markup;
import eu.elg.model.Request;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Request representing a still image - the actual image data will be sent as a separate part of a multipart
 * request.
 */
public class ImageRequest extends BinaryRequest<ImageRequest> {

  /**
   * Enumeration of available image formats.
   */
  public static enum Format {
    TIFF, PNG, JPEG, GIF, BMP
  }

  private Format format;
  private Map<String, Object> features;

  /**
   * The image format of the request data - optional if the data is self-describing.
   */
  @JsonProperty("format")
  @JsonInclude(JsonInclude.Include.NON_NULL)
  public Format getFormat() {
    return format;
  }

  /**
   * Set the data format, if the data is not in a format with a self-describing header.
   *
   * @param format the format of the image data.
   */
  @JsonProperty("format")
  public void setFormat(Format format) {
    this.format = format;
  }

  /**
   * Fluent setter for the {@link #getFormat format property}
   *
   * @param format the format of the image data
   * @return this object, for chaining
   */
  public ImageRequest withFormat(Format format) {
    setFormat(format);
    return this;
  }

  /**
   * Features of this request.  The keys in the map must be strings, the values can be anything Jackson knows how to
   * serialise to JSON.
   */
  @JsonProperty("features")
  public Map<String, Object> getFeatures() {
    return features;
  }

  /**
   * Set features for this request.The keys in the map must be strings, the values can be anything Jackson knows how
   * to serialise to JSON.
   *
   * @param features map of features to set
   */
  @JsonProperty("features")
  @JsonInclude(JsonInclude.Include.NON_EMPTY)
  public void setFeatures(Map<String, Object> features) {
    this.features = features;
  }

  /**
   * Fluent setter for the {@link #getFeatures features property}
   *
   * @param features map of features to set
   * @return this object, for chaining.
   */
  public ImageRequest withFeatures(Map<String, Object> features) {
    setFeatures(features);
    return this;
  }

  /**
   * Shorthand way to set features - this method takes an alternating list of key1, value1, key2, value2, etc.  Keys
   * must be strings, values can be anything Jackson knows how to serialise to JSON.
   *
   * @param keysAndValues alternating list of key, value, key, value, ...
   * @return this object, for chaining
   */
  public ImageRequest withFeatures(Object... keysAndValues) {
    if(keysAndValues.length % 2 != 0) {
      throw new IllegalArgumentException("argument must be alternating keys and values");
    }
    Map<String, Object> featuresMap = new LinkedHashMap<>();
    for(int i = 0; i < keysAndValues.length; i += 2) {
      featuresMap.put((String) keysAndValues[i], keysAndValues[i + 1]);
    }
    return withFeatures(featuresMap);
  }

  /**
   * Convenience fluent setter for a single key/value feature pair.  This method calls {@link Map#put(Object, Object)
   * put} on the current {@link #getFeatures() features} map, creating a new {@link LinkedHashMap} if this is null.  If
   * the current features map is not mutable then this call may fail.
   *
   * @param key   the feature key
   * @param value the feature value (can by any type that Jackson knows how to serialise)
   * @return this object, for chaining.
   */
  public ImageRequest withFeature(String key, Object value) {
    if(getFeatures() == null) {
      setFeatures(new LinkedHashMap<>());
    }
    getFeatures().put(key, value);
    return this;
  }

  public String type() {
    return "image";
  }
}
