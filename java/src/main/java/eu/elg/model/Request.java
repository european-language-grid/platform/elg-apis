/*
 *    Copyright 2019 The European Language Grid
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package eu.elg.model;

import com.fasterxml.jackson.annotation.*;
import eu.elg.model.requests.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Representation of a service invocation request.
 *
 * @param <T> subclasses should instantiate this with their own type, similar to <code>java.lang.Enum</code>
 */
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "type"
)
@JsonSubTypes({
        @JsonSubTypes.Type(name = "text", value = TextRequest.class),
        @JsonSubTypes.Type(name = "audio", value = AudioRequest.class),
        @JsonSubTypes.Type(name = "structuredText", value = StructuredTextRequest.class),
        @JsonSubTypes.Type(name = "image", value = ImageRequest.class),
})
public class Request<T extends Request<T>> extends WarnForUnknownProperties {

  private Map<String, Object> params;

  /**
   * Additional parameters, specific to the target service - this is a catch-all for request parameters that do not fit
   * into the standard vendor-neutral request schema.
   */
  @JsonProperty("params")
  @JsonInclude(JsonInclude.Include.NON_NULL)
  public Map<String, Object> getParams() {
    return params;
  }

  /**
   * Set the additional parameters for this request, specific to the target service.
   * @param params parameters to set
   */
  @JsonProperty("params")
  public void setParams(Map<String, Object> params) {
    this.params = params;
  }

  /**
   * Fluent setter for the {@link #getParams params property}
   * @param params parameters to set
   * @return this object, for chaining
   */
  public T withParams(Map<String, Object> params) {
    setParams(params);
    return (T) this;
  }

  /**
   * Return the type of this request as specified in the JSON - should be overridden by subclasses.
   */
  public String type() {
    return "unknown";
  }


  @Override
  protected Function<String, StatusMessage> propertyNameToMessage() {
    return StandardMessages::elgRequestPropertyUnsupported;
  }

}
