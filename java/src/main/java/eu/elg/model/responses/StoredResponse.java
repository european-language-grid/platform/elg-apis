/*
 *    Copyright 2019 The European Language Grid
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package eu.elg.model.responses;

import com.fasterxml.jackson.annotation.JsonProperty;
import eu.elg.model.Response;

import java.net.URI;

/**
 * This class represents a pointer telling the receiver where to look for the real message content.
 */
public class StoredResponse extends Response<StoredResponse> {

  private URI uri;

  /**
   * The URI of the real response in object storage.
   */
  @JsonProperty("uri")
  public URI getUri() {
    return uri;
  }

  /**
   * Set the location in object storage of the real response.
   * @param uri the real response location
   */
  @JsonProperty("uri")
  public void setUri(URI uri) {
    this.uri = uri;
  }

  /**
   * Fluent setter for the {@link #getUri URI property}
   * @param uri the real response location
   * @return this object, for chaining
   */
  public StoredResponse withUri(URI uri) {
    setUri(uri);
    return this;
  }

  /**
   * Fluent setter for the {@link #getUri URI property}, taking a URI string instead of a {@link URI}
   * @param uri the real response location
   * @return this object, for chaining
   * @throws IllegalArgumentException if the specified string is not a valid URI.
   */
  public StoredResponse withUri(String uri) {
    return withUri(URI.create(uri));
  }
}
