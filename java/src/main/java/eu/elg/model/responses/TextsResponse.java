/*
 *    Copyright 2019 The European Language Grid
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package eu.elg.model.responses;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import eu.elg.model.*;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

/**
 * Response consisting of a set of one or more new texts, each with optional annotations attached to it, for example a
 * set of possible translations produced by a translation tool or possible transcriptions produced by a speech-to-text
 * recogniser.
 */
public class TextsResponse extends Response<TextsResponse> {
  private List<Text> texts;

  /**
   * The texts in this response.
   */
  @JsonProperty("texts")
  public List<Text> getTexts() {
    return texts;
  }

  /**
   * Set the texts in this response.
   *
   * @param texts texts to return
   */
  @JsonProperty("texts")
  public void setTexts(List<Text> texts) {
    this.texts = texts;
  }

  /**
   * Fluent setter for the {@link #getTexts texts property}
   *
   * @param texts texts to return
   * @return this object, for chaining
   */
  public TextsResponse withTexts(List<Text> texts) {
    setTexts(texts);
    return this;
  }

  /**
   * Fluent setter for the {@link #getTexts texts property} taking multiple arguments rather than a single list, to
   * support constructs like <code>new TextsResponse().withTexts(new Text()....)</code>
   *
   * @param texts texts to return
   * @return this object, for chaining
   */
  public TextsResponse withTexts(Text... texts) {
    return withTexts(Arrays.asList(texts));
  }

  /**
   * A single text in a (potentially) multiple texts response.  Each text can have an associated score (for example a
   * confidence value for multiple alternative translations or transcriptions) and optional annotations, which can be
   * linked to both the result text in this object and to the original source material from the corresponding request.
   */
  public static class Text extends WarnForUnknownProperties {
    private String role;

    private String content;

    private List<Text> texts;

    private Number score;

    private Markup markup = new Markup();

    /**
     * An optional role for this object within the response, for example if the response is a list of possible
     * alternative translations of the same source text then the role could be "alternative", if the response is a list
     * of sentences then the role could be "sentence" (and each sentence may itself have a list of "alternative"
     * Texts).
     */
    @JsonProperty("role")
    public String getRole() {
      return role;
    }

    /**
     * Set the role for this text.
     *
     * @param role the role value.
     */
    @JsonProperty("role")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public void setRole(String role) {
      this.role = role;
    }

    /**
     * Fluent setter for the {@link #getRole role property}
     *
     * @param role the role value.
     * @return this object, for chaining
     */
    public Text withRole(String role) {
      setRole(role);
      return this;
    }


    /**
     * The content of this response.  Either this property or "texts" should be set but not both.
     */
    @JsonProperty("content")
    public String getContent() {
      return content;
    }

    /**
     * Set the content to return.
     *
     * @param content the content of this response
     */
    @JsonProperty("content")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public void setContent(String content) {
      this.content = content;
    }

    /**
     * Fluent setter for the {@link #getContent content property}
     *
     * @param content the content of this response
     * @return this object, for chaining.
     */
    public Text withContent(String content) {
      setContent(content);
      return this;
    }

    /**
     * The texts in this response.
     */
    @JsonProperty("texts")
    public List<Text> getTexts() {
      return texts;
    }

    /**
     * Set the texts in this response.
     *
     * @param texts texts to return
     */
    @JsonProperty("texts")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public void setTexts(List<Text> texts) {
      this.texts = texts;
    }

    /**
     * Fluent setter for the {@link #getTexts texts property}
     *
     * @param texts texts to return
     * @return this object, for chaining
     */
    public Text withTexts(List<Text> texts) {
      setTexts(texts);
      return this;
    }

    /**
     * Fluent setter for the {@link #getTexts texts property} taking multiple arguments rather than a single list, to
     * support constructs like <code>new Text().withTexts(new Text()....)</code>
     *
     * @param texts texts to return
     * @return this object, for chaining
     */
    public Text withTexts(Text... texts) {
      return withTexts(Arrays.asList(texts));
    }

    /**
     * An optional confidence score for this text, for example if the response is a list of possible alternative
     * transcriptions from a single piece of audio, or alternative translations of the same source text.
     */
    @JsonProperty("score")
    public Number getScore() {
      return score;
    }

    /**
     * Set the confidence score for this text.
     *
     * @param score the score value - no particular significance is attached to this value, and there is no prescribed
     *              range within which it must fall.
     */
    @JsonProperty("score")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public void setScore(Number score) {
      this.score = score;
    }

    /**
     * Fluent setter for the {@link #getScore score property}
     *
     * @param score the score value - no particular significance is attached to this value, and there is no prescribed
     *              range within which it must fall.
     * @return this object, for chaining
     */
    public Text withScore(Number score) {
      setScore(score);
      return this;
    }

    /**
     * The markup (annotations and features) on this text.
     * @return the current markup.
     */
    @JsonUnwrapped
    public Markup getMarkup() {
      return markup;
    }

    /**
     * Set the markup on this text.
     * @param markup the markup to use.
     */
    @JsonUnwrapped
    public void setMarkup(Markup markup) {
      this.markup = markup;
    }

    /**
     * Fluent setter for the {@link #getMarkup() markup} property.
     *
     * @param markup the markup to use
     * @return this object, for chaining
     */
    public Text withMarkup(Markup markup) {
      setMarkup(markup);
      return this;
    }

    @Override
    protected Function<String, StatusMessage> propertyNameToMessage() {
      return StandardMessages::elgResponseTextsPropertyUnsupported;
    }

    @Override
    public List<StatusMessage> unknownPropertyWarnings() {
      return combineUnknownPropertyWarnings(super.unknownPropertyWarnings(), texts);
    }

    @Override
    protected boolean validPropertyName(String propName) {
      return Markup.validPropertyName(propName);
    }
  }

  @Override
  public List<StatusMessage> unknownPropertyWarnings() {
    return combineUnknownPropertyWarnings(super.unknownPropertyWarnings(), texts);
  }

}
