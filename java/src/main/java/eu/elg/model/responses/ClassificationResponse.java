/*
 *    Copyright 2019 The European Language Grid
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package eu.elg.model.responses;

import com.fasterxml.jackson.annotation.JsonProperty;
import eu.elg.model.Response;
import eu.elg.model.StatusMessage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Response encapsulating one or more classifications of the whole input message, optionally with confidence scores
 * attached.
 */
public class ClassificationResponse extends Response<ClassificationResponse> {
  private List<ClassificationClass> classes;

  /**
   * The list of classes.  This list would typically be ordered with the most likely class first but this is not
   * strictly required.
   */
  @JsonProperty("classes")
  public List<ClassificationClass> getClasses() {
    return classes;
  }

  /**
   * Set the list of classes.
   *
   * @param classes list of classes
   */
  @JsonProperty("classes")
  public void setClasses(List<ClassificationClass> classes) {
    this.classes = classes;
  }

  /**
   * Fluent setter for the {@link #getClasses classes property}
   *
   * @param classes list of classes
   * @return this object, for chaining.
   */
  public ClassificationResponse withClasses(List<ClassificationClass> classes) {
    setClasses(classes);
    return this;
  }

  /**
   * Fluent setter for the {@link #getClasses classes property}, taking multiple parameters rather than a single list to
   * allow for constructs like <code>new ClassificationResponse().withClasses(new ClassificationClass()....)</code>
   *
   * @param classes list of classes
   * @return this object, for chaining.
   */
  public ClassificationResponse withClasses(ClassificationClass... classes) {
    return withClasses(new ArrayList<>(Arrays.asList(classes)));
  }

  @Override
  public List<StatusMessage> unknownPropertyWarnings() {
    return combineUnknownPropertyWarnings(super.unknownPropertyWarnings(), classes);
  }
}
