/*
 *    Copyright 2022 The European Language Grid
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package eu.elg.model.util;

import eu.elg.model.AnnotationObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Helper class to simplify handling of annotation offsets in text.  The ELG API
 * specifications count annotation offsets in terms of Unicode characters - what
 * Java refers to as "code points" - so supplementary characters such as Emoji
 * count as one "place" in the text content.  However Java strings and all the
 * associated machinery for things like regular expressions are based around the
 * UTF-16 representation in which supplementary characters count as <em>two</em>
 * places and are represented in memory as a "surrogate pair" of 16 bit
 * <code>char</code> values.  This class helps to deal with the mapping from one
 * scheme to the other.
 */
public class TextOffsetsHelper {

  /*
   * Algorithm:
   *
   * This class maintains a list of "cut points" which are the offsets immediately *after* each
   * supplementary character.  The chars and codepoints arrays are parallel, with matching indexes
   * holding the "Java" offset (UTF-16 chars) and "ELG" offset (code points) of that cut point.
   * At offsets strictly *between* cut points, one UTF-16 char is one code point.
   *
   * To convert from one co-ordinate system to the other, find the last cut point at or before
   * the required source offset N - call the offset of that cut point M (M <= N).  The target offset
   * in the other co-ordinate system is the target side of the same cut point, plus N, minus M
   */

  private final int[] chars;
  private final int[] codepoints;

  /**
   * Create a helper to convert offsets between Java and ELG co-ordinate systems for the given text content.
   *
   * @param content the text within which we need to convert offsets.
   */
  public TextOffsetsHelper(String content) {
    List<Integer> charsList = new ArrayList<>();
    List<Integer> cpList = new ArrayList<>();
    int ch = 0;
    int cp = 0;
    // add an initial 0,0 cut point to simplify the lookup logic
    charsList.add(ch);
    cpList.add(cp);
    while(ch < content.length()) {
      if(Character.isSupplementaryCodePoint(content.codePointAt(ch))) {
        // This is a supplementary character - move forward by *two* char places and add a cut point
        ch += 2;
        cp += 1;
        charsList.add(ch);
        cpList.add(cp);
      } else {
        // not a supplementary, so one char = one codepoint and no cut point required
        ch += 1;
        cp += 1;
      }
    }
    chars = charsList.stream().mapToInt(i -> i).toArray();
    codepoints = cpList.stream().mapToInt(i -> i).toArray();
  }

  /**
   * Given an ELG API annotation offset (counting in Unicode code points), return the corresponding offset in chars
   * that points to the same position in the text as a Java String.
   *
   * @param elgOffset offset in code points, as taken from an {@link AnnotationObject}
   * @return offset in UTF-16 <code>char</code>s that points to the relevant place in the Java String.
   */
  public int elgToJava(int elgOffset) {
    return convert(elgOffset, codepoints, chars);
  }

  /**
   * Given a Java String offset into the text (counting in UTF-16 <code>char</code>s), return the corresponding offset
   * in Unicode code points that would be used by an ELG API {@link AnnotationObject} to point to the
   * same location.
   *
   * @param javaOffset Java String offset in UTF-16 chars
   * @return equivalent position counting in Unicode code points, suitable for use as a start or end offset in an
   * {@link eu.elg.model.AnnotationObject}
   */
  public int javaToElg(int javaOffset) {
    return convert(javaOffset, chars, codepoints);
  }

  private int convert(int offset, int[] from, int[] to) {
    if(offset < 0) {
      throw new StringIndexOutOfBoundsException(offset);
    }
    int adjustmentIdx = Arrays.binarySearch(from, offset);
    if(adjustmentIdx >= 0) {
      // exact match for a cut point
      return to[adjustmentIdx];
    } else {
      // between cut points:
      // binarySearch returns (-x - 1) where x is the index at which you would insert elgOffset
      // to maintain the order, so (-x - 2) is the position of the previous cut point
      int previousCutPoint = -adjustmentIdx - 2;
      // target offset is that previous cut point in the target array plus the number of places
      // that we have moved past that cut point position
      return to[previousCutPoint] + (offset - from[previousCutPoint]);
    }
  }

  /**
   * Convenience method to create an {@link AnnotationObject} with start and end offsets converted from a given pair
   * of Java String indices.  For example:
   *
   * <pre><code>
   *   Matcher m = MY_PATTERN.matcher(request.getContent());
   *   TextOffsetsHelper helper = new TextOffsetsHelper(request.getContent());
   *   while(m.find()) {
   *     annotations.add(helper.annotationWithOffsets(m.start(), m.end()).withFeature("example", "yes");
   *   }
   * </code></pre>
   * @param start start offset of the annotation, in terms of Java string indices
   * @param end end offset of the annotation, in terms of Java string indices
   * @return new annotation with the given offsets mapped to the ELG API co-ordinate system.
   */
  public AnnotationObject annotationWithOffsets(int start, int end) {
    return new AnnotationObject().withOffsets(javaToElg(start), javaToElg(end));
  }
}
