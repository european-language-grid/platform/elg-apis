/*
 *    Copyright 2019 The European Language Grid
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package eu.elg.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Arrays;
import java.util.List;

/**
 * Details of a failed task, containing a list of {@link StatusMessage}s.
 */
public class Failure {

  private List<StatusMessage> errors;

  /**
   * Messages describing the failure.
   */
  @JsonProperty("errors")
  public List<StatusMessage> getErrors() {
    return errors;
  }

  /**
   * Set the list of messages describing this failure.
   *
   * @param errors error messages.
   */
  @JsonProperty("errors")
  public void setErrors(List<StatusMessage> errors) {
    this.errors = errors;
  }

  /**
   * Fluent setter for the {@link #getErrors errors property}
   *
   * @param errors error messages
   * @return this object, for chaining.
   */
  public Failure withErrors(List<StatusMessage> errors) {
    setErrors(errors);
    return this;
  }

  /**
   * Fluent setter for the {@link #getErrors errors property}, taking multiple parameters instead of a single List to
   * allow constructs like <code>new Failure().withErrors(new StatusMessage()....)</code>
   *
   * @param errors error messages
   * @return this object, for chaining
   */
  public Failure withErrors(StatusMessage... errors) {
    return withErrors(Arrays.asList(errors));
  }

  /**
   * Wrap this response in a message.
   * @return a {@link ResponseMessage} wrapping this response
   */
  public ResponseMessage asMessage() {
    ResponseMessage msg = new ResponseMessage();
    msg.setFailure(this);
    return msg;
  }

}
