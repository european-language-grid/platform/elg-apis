/*
 *    Copyright 2019 The European Language Grid
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package eu.elg.model;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Utility superclass for various parts of the model to generate warning messages when unknown properties are
 * encountered in the JSON.
 */
public abstract class WarnForUnknownProperties {
  /**
   * Gather the names of any unrecognised properties in the JSON.
   */
  @JsonIgnore
  private List<String> unknownProperties;

  /**
   * Called whenever an unrecognised property is found in the JSON.  The names of such properties are gathered up so
   * they can be converted to warnings.
   *
   * @param name  name of the unrecognised property
   * @param value property value, ignored.
   */
  @JsonAnySetter
  public void addUnknownProperty(String name, Object value) {
    if(!validPropertyName(name)) {
      if(unknownProperties == null) {
        unknownProperties = new ArrayList<>();
      }
      unknownProperties.add(name);
    }
  }

  /**
   * Generate a list of warning messages for any unsupported property names in this response.
   *
   * @return a list of messages suitable for use as warnings, or null if there were no unknown properties.
   */
  public List<StatusMessage> unknownPropertyWarnings() {
    if(unknownProperties == null) {
      return null;
    } else {
      return unknownProperties.stream().map(propertyNameToMessage()).collect(Collectors.toList());
    }
  }

  /**
   * Helper method that can be used by subclasses that need to implement {@link #unknownPropertyWarnings()} to gather
   * unknown property warnings from child elements as well as top-level ones of their own.
   *
   * @param baseWarnings base set of warnings, typically from <code>super.unknownPropertyWarnings()</code>, may be null
   * @param children     child elements from which warnings should also be collected.
   * @return null if baseWarnings is null and none of the children have any warnings, otherwise a single list combining
   * the warnings from the bast list and the children.
   */
  protected static List<StatusMessage> combineUnknownPropertyWarnings(List<StatusMessage> baseWarnings,
                                                                      Iterable<? extends WarnForUnknownProperties> children) {
    // This logic looks a bit odd but basically I'm trying to avoid copying unless I really have to.
    // If there's no warnings anywhere, return null.
    // If there's warnings in just this text or just *one* of its children, then return that as-is,
    // If there's warnings in more than one place, then construct a new list and add all those different
    // sets of warnings to that.
    List<StatusMessage> toReturn = baseWarnings;
    int numWarningLists = (toReturn == null) ? 0 : 1;
    if(children != null) {
      for(WarnForUnknownProperties c : children) {
        List<StatusMessage> childWarnings = c.unknownPropertyWarnings();
        if(childWarnings != null) {
          if(numWarningLists == 0) {
            toReturn = childWarnings;
          } else {
            if(numWarningLists == 1) {
              toReturn = new ArrayList<>(toReturn);
            }
            toReturn.addAll(childWarnings);
          }
          numWarningLists++;
        }
      }
    }

    return toReturn;
  }

  /**
   * Function to convert a single property name to a StatusMessage that could be included in the warnings list.
   *
   * @return function that takes one unrecognised property name and returns a {@link StatusMessage} - typically a method
   * reference to one of the {@link StandardMessages}
   */
  protected abstract Function<String, StatusMessage> propertyNameToMessage();

  /**
   * Filter function that will be passed each potentially-unknown property to check whether it is genuinely
   * invalid or actually a valid property, but in a way Jackson cannot see (e.g. it is handled by
   * <code>JsonUnwrapped</code>).  The default implementation always returns <code>false</code>
   *
   * @param propName the property name to check
   * @return true if this is in fact a valid property name, false if it is not valid and should generate a warning.
   */
  protected boolean validPropertyName(String propName) {
    return false;
  }
}
